package com.luv2code.hibernate.demo;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class DeleteDemo {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
										.configure("hibernate.cfg.xml")
										.addAnnotatedClass(Instructor.class)
										.addAnnotatedClass(InstructorDetail.class)
										.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			//get instructor by id
			Instructor tempInstructor = session.get(Instructor.class, 2);
			System.out.println(tempInstructor);
			
			if(tempInstructor != null)
				session.delete(tempInstructor);
			
			//delete instructor selected by id 
	
			
			session.getTransaction().commit();
			
		} finally {
			// factory close
			factory.close();
		}
		
	}

}
 